# Install dependencies only when needed
FROM node:16-alpine AS deps
WORKDIR /apps
COPY package.json package-lock.json ./
RUN npm install --frozen-lockfile

# Rebuild the source code only when needed
FROM node:16-alpine AS builder
WORKDIR /apps
COPY --from=deps /apps/node_modules ./node_modules
COPY . .
RUN npm run build

# Production image, copy all the files and run next
FROM node:16-alpine AS runner
WORKDIR /apps

ENV NODE_ENV production

RUN addgroup --system --gid 1001 nodejs
RUN adduser --system --uid 1001 nextjs

COPY --from=builder /apps/package.json ./package.json

COPY --from=deps /apps/node_modules ./node_modules
COPY --from=builder --chown=nextjs:nodejs /apps/.next ./.next

USER nextjs
EXPOSE 3000
ENV PORT 3000
CMD ["npm", "start"]

